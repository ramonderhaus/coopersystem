# Projeto de seleção Coopersystem (Angular 12+)

O teste consiste em criar uma mini aplicação em `Angular 12+` que simula um resgate personalizado de fundos de investimentos em ações.

Serão avaliados:

- Integração com API Rest
- Navegação entre telas
- Validação de formulários conforme regras de negócios
- Padrões de codificação
- Testes unitários com karma/jasmine (Pode enviar o desafio sem os testes, e realizar um commit com os testes posteriormente)

## Recursos utilizados

- Angular 13
- Lazy loading
- Route transition animations
- Internationalization @ngx-translate
- @swimlane/ngx-datatable
- Design Responsivo
- Bootstrap 5
- ngx-bootstrap
- ngx-currency
- ngx-mask
- ngx-toastr

## Uso

Clone os arquivos de origem e navegue até o diretório raiz. Execute `npm install` e então execute `npm run start`. Para visualizar o app no navegador, digite `localhost:4200`.
