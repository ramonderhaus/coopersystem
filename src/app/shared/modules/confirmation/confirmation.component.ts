import {Component, Input, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  @Input() contentMessage!: string;
  @Input() subMessage!: string;
  @Input() confirmButtonLabel!: string;
  @Input() cancelButtonLabel!: string;
  @Input() classType: string = 'primary';

  @Output() confirmClicked = new Subject();
  @Output() cancelClicked = new Subject();

  constructor() { }

  ngOnInit() { }

}
