import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxCurrencyModule } from "ngx-currency";

@NgModule({
  imports: [
    CommonModule,
    NgxCurrencyModule
  ],
  exports: [
    NgxCurrencyModule
  ]
})
export class CurrencyModule {}
