export interface IInvestments {
  response: IResponse
}

interface IResponse {
  status: number
  data: {
    listaInvestimentos: IInvestmentsList[]
  }
}

export interface IInvestmentsList {
  nome: string;
  objetivo: string;
  saldoTotal: number;
  indicadorCarencia: string;
  acoes: IAcoes[]
}

export interface IAcoes {
  id: number;
  nome: string;
  percentual: number;
  valor?: number;
  resgate?: number;
}
