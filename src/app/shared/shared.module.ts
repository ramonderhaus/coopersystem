import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BootstrapModule } from '@shared/modules/bootstrap/bootstrap.module';
import { CooperSystemFormsModule } from '@app/shared/modules/forms/coopersystem-forms.module';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NgSelectModule } from '@ng-select/ng-select';
import { DatatableModule } from '@app/shared/modules/ngx-datatable/datatable.module';
import { ToastrModule } from 'ngx-toastr';
import { ConfirmationModule } from '@shared/modules/confirmation/confirmation.module';
import { NgxTranslateModule } from '@shared/modules/ngx-translate/ngx-translate.module';
import { CurrencyModule } from '@shared/modules/ngx-currency/datatable.module';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BootstrapModule,
    ConfirmationModule,
    DatatableModule,
    CooperSystemFormsModule,
    NgSelectModule,
    NgxMaskModule.forRoot(maskConfig),
    ToastrModule.forRoot(),
    NgxTranslateModule,
    CurrencyModule
  ],
  exports: [
    BootstrapModule,
    ConfirmationModule,
    DatatableModule,
    CooperSystemFormsModule,
    NgSelectModule,
    NgxMaskModule,
    ToastrModule,
    NgxTranslateModule,
    CurrencyModule
  ]
})
export class SharedModule { }
