import { animate, animateChild, group, query, style, transition, trigger, } from '@angular/animations';

/**
 * Animação para a transição de rotas
 */
export const customAnimations = [
  trigger('routerTransitionFade', [
    transition('* => *',
      group([
        query('app-content > :enter, app-content > :leave ', [
          style({
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
          }),
        ], { optional: true }),
        query('app-content > :leave', [
          style({ opacity: 1 }),
          animate('0s ease', style({ opacity: 0 })),
        ], { optional: true }),
        query('app-content > :enter', [
          style({ opacity: 0 }),
          animate('0.75s ease', style({ opacity: 1 })),
        ], { optional: true }),
        query('app-content > :leave', animateChild(), { optional: true }),
        query('app-content > :enter', animateChild(), { optional: true }),
      ])
    ),
  ]),
];
