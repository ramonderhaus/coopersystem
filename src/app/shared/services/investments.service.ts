import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { IInvestments } from '@shared/models/investments';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class InvestmentsService {

  /**
   * Investments Api Http Rest
   */
  url = environment.investmentsApiUrl;

  /**
   * Headers
   */
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  /**
   * @param httpClient Injetando o HttpClient
   */
  constructor(
    private httpClient: HttpClient
  ) { }

  /**
   * Requisição para os dados dos investimentos
   * @returns Os investimentos
   */
  getInvestments(): Observable<IInvestments> {
    return this.httpClient.get<IInvestments>(this.url)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Código do erro: ${error.status}, ` + `mensagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };
}
