import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { customAnimations } from '@shared/helper/animations';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  animations: customAnimations,
})
export class ContentComponent implements OnInit, OnDestroy {

  /**
   * Aplica a animação ao mudar a rota
   */
  @HostBinding('@routerTransitionFade') routeAnimationFade = false;

  /**
   * Verifica se mudou a rota
   */
  onNavigationEnd: Subscription;

  constructor(
    private router: Router
  ) {
    this.onNavigationEnd = this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd)
      )
      .subscribe(() => (this.routeAnimationFade = !this.routeAnimationFade));
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.onNavigationEnd.unsubscribe();
  }
}
