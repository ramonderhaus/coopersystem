import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { ContentComponent } from './content/content.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SharedModule } from '@shared/shared.module';


@NgModule({
  declarations: [
    MainComponent,
    ContentComponent,
    ToolbarComponent,
    NavigationComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule
  ],
  exports:[
    MainComponent,
    SharedModule
  ]
})
export class MainModule { }
