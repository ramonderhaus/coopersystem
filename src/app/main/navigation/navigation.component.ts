import { AfterContentChecked, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, AfterContentChecked {

  /**
   * Armazena a url atual
   */
  url!: string;

  /**
   * Controla a exibição do menu
   */
  isShowNavbar: boolean = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void { }

  /**
   * Aplica a url atual ao mudar a rota
   */
  ngAfterContentChecked(): void {
    this.url = this.getRoute();
  }

  /**
   *
   * @returns Retorna a primeira parte da url atual
   */
  getRoute(): string {
    return this.router.url.split('/')[1];
  }

  showNavbar(): void {
    this.isShowNavbar = !this.isShowNavbar
  }

}
