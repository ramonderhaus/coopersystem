import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@env/environment';

interface IFlags {
  lang: string
}

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  flags: IFlags[] = [
    { lang: 'pt-br' },
    { lang: 'en' }
  ];

  selectedLang: string;

  constructor(
    private translate: TranslateService
  ) {
    this.selectedLang = environment.defaultLanguage;
  }

  ngOnInit(): void { }

  /**
   * Altera o idioma do sistema
   * @param item Define o idioma selecionado
   */
  changeLanguage(item: IFlags): void {
    this.translate.use(item.lang);
  }

}
