import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvestmentsComponent } from './investments.component';

const routes: Routes = [
  { path: '', component: InvestmentsComponent },
  {
    path: 'edit/:id',
    loadChildren: () =>
      import('./action/investments-action.module').then(m => m.InvestmentsActionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvestmentsRoutingModule { }
