import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestmentsActionComponent } from './investments-action.component';

describe('InvestmentsActionComponent', () => {
  let component: InvestmentsActionComponent;
  let fixture: ComponentFixture<InvestmentsActionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvestmentsActionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestmentsActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
