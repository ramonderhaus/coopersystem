import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InvestmentsActionComponent } from './investments-action.component';

const routes: Routes = [
  { path: '', component: InvestmentsActionComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvestmentsActionRoutingModule { }
