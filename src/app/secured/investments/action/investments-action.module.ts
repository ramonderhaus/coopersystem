import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { InvestmentsActionRoutingModule } from './investments-action-routing.module';
import { InvestmentsActionComponent } from './investments-action.component';
import { SharedModule } from '@shared/shared.module';
import { TranslateLoader, TranslateModule, TranslateService, } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [InvestmentsActionComponent],
  imports: [
    CommonModule,
    InvestmentsActionRoutingModule,
    SharedModule,
    TranslateModule.forChild({
      defaultLanguage: 'pt-br',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      extend: true,
    }),
  ],
  exports: [InvestmentsActionComponent],
})
export class InvestmentsActionModule {
  constructor(protected translateService: TranslateService) {
    const currentLang = translateService.currentLang;
    translateService.use(currentLang);
  }
}
