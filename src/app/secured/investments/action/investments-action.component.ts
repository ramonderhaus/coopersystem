import { Component, OnInit, OnDestroy } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationComponent } from '@app/shared/modules/confirmation/confirmation.component';
import { IAcoes, IInvestments, IInvestmentsList } from '@shared/models/investments';
import { InvestmentsService } from '@shared/services/investments.service';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

interface IModalMessage {
  contentMessage: string
  subMessage?: string
  classType: string
  cancelButtonLabel?: string
  confirmButtonLabel: string
  modalClass?: string
}

@Component({
  selector: 'app-investments-action',
  templateUrl: './investments-action.component.html',
  styleUrls: ['./investments-action.component.scss'],
  providers: [CurrencyPipe, InvestmentsService],
})
export class InvestmentsActionComponent implements OnInit, OnDestroy {

  /**
   * Recebe o 'id' do investimento na url
   */
  id: number;

  /**
   * Cria o form
   */
  form!: FormGroup;

  /**
   * Recebe a lista de investimentos
   */
  investments!: IInvestmentsList | null;

  /**
   * Recebe os dados da requisição dos investimentos
   */
  investmentsServiceEnd$!: Subscription;

  /**
   * Cancela as assinaturas dos observables
   */
  subscriptions: Subscription[] = [];

  /**
   * Controla a exibição do conteúdo no HTML
   */
  loading: boolean = true;

  /**
   * Retorna acoes como FormArray
   */
  get acoes() {
    return this.form.get('acoes') as FormArray;
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private cp: CurrencyPipe,
    private bsModalService: BsModalService,
    private formBuilder: FormBuilder,
    private router: Router,
    private investmentsService: InvestmentsService,
    private toastr: ToastrService,
    protected translate: TranslateService
  ) {
    this.id = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.createInvestimentsForm();
    this.getInvestmnets();
  }

  /**
   * Chama o serviço para obter todos os investimentos
   */
  getInvestmnets(): void {
    this.investmentsServiceEnd$ = this.investmentsService.getInvestments().subscribe((investments: IInvestments) => {
      this.investments = investments.response.data.listaInvestimentos[this.id];
      if(!this.investments || this.investments.indicadorCarencia === 'S') {
        this.goBack()
      }
      this.loading = false;
      this.loadValues();
    });
    this.subscriptions.push(this.investmentsServiceEnd$)
  }

  /**
   * Define os campos do form
   */
   createInvestimentsForm(): void {
    this.form = this.formBuilder.group({
      acoes: this.formBuilder.array([])
    });
  }

  /**
   * Define os campos do form acoes
   */
  addFormAcoes(): FormGroup {
    return this.formBuilder.group({
      nome: [null],
      percentual: [null],
      valor: [null],
      resgate: [null]
    });
  }

  /**
   * Adiciona um item no form acoes
   */
  addForm(): void {
    this.acoes.push(this.addFormAcoes());
  }

  /**
   * Preenche os dados do cliente no form, se tiver id na url
   */
  loadValues(): void {
    this.investments!.acoes.forEach((data, index) => {
      this.addForm();
      const {nome, percentual} = data;
      this.acoes.at(index).patchValue({
        nome: nome,
        percentual: percentual,
        valor: Number((this.investments!.saldoTotal * percentual / 100).toFixed(2)),
        resgate: null
      });
      this.acoes.at(index).get('resgate')!.setValidators([Validators.max(this.acoes.at(index).get('valor')!.value)]);
    });
    this.acoes.markAllAsTouched();
  }

  /**
   * Retorna o valor total a ser resgatado
   */
  getTotalResgate(): number {
    return this.acoes.controls.reduce((soma, acao) => soma + acao.value['resgate'], 0)
  }

  /**
   * Retorna para a listagem de investimentos
   */
  goBack(): void {
    this.router.navigate(['/investments']);
  }

  /**
   * Verifica o status do form e define os textos do modal
   * @param status Status do form
   * @returns Mensagens exibidas no modal
   */
  getModalMessage(status: string): IModalMessage {
    let message: IModalMessage;

    switch(status) {
      case 'VALID':
        message = {
          contentMessage: this.translate.instant('ValidValuesMessageBody'),
          classType: 'info',
          confirmButtonLabel: this.translate.instant('ValidValuesMessageButton')
        };
        break;

      case 'INVALID':
        const invalidValues = this.setInvalidValuesMessage(this.getInvalidValues());
        message = {
          contentMessage: this.translate.instant('InvalidValuesMessageTitle'),
          subMessage: this.translate.instant('InvalidValuesMessageBody') + invalidValues,
          classType: 'danger',
          confirmButtonLabel: this.translate.instant('InvalidValuesMessageButton'),
          modalClass: 'modal-lg'
        };
        break;
    }

    return message!;
  }

  /**
   * Filtra as ações com valores inválidos para resgate
   * @returns Valor inválido de resgate
   */
  getInvalidValues(): IAcoes[] {
    const invalidValues = this.form.getRawValue()['acoes'].filter((form: IAcoes) => form.valor! < form.resgate!);
    return invalidValues;
  }

  /**
   * Cria a estrutura HTML, com as ações com valores inválidos para resgate, para exibição no modal
   * @param values Objeto com os dados da ação
   * @returns Estrutura HTML com a mensagem
   */
  setInvalidValuesMessage(values: IAcoes[]): string {
    let message: string = `<div class="mt-4">`;
    values.forEach((value) => {
      message += `
        <div class="mt-1">
          <span class="fw-600">${value.nome}:</span> ${this.translate.instant('AmountCantBeMore')} ${this.cp.transform(value.valor,'BRL')}.
        </div>`
    });
    return message + `</div>`;
  }


  /**
   * Confirma o resgate dos valores
   * @returns Retorna false, se o form for inválido
   */
  redeemInvestmen(): void {
    if (this.getTotalResgate() === 0) {
      this.toastr.error(this.translate.instant('FillInAValidAmountToRedeem'));
      return;
    }

    const { contentMessage, subMessage, classType, confirmButtonLabel, modalClass } = this.getModalMessage(this.acoes.status);

    const opts: ModalOptions = {
      class: 'modal-dialog-centered ' + modalClass,
      ignoreBackdropClick: true,
    };

    const modal = this.bsModalService.show(ConfirmationComponent, opts);
    const componentInstance: ConfirmationComponent = modal.content!;

    componentInstance.contentMessage = contentMessage;
    componentInstance.subMessage = subMessage!;
    componentInstance.classType = classType;
    componentInstance.confirmButtonLabel = confirmButtonLabel;

    componentInstance.confirmClicked.subscribe(() => {
      modal.hide();
      if (this.acoes.valid) {
        this.goBack();
      }
    });

    componentInstance.cancelClicked.subscribe(() => {
      modal.hide();
    });
  }

  /**
   * Desinscreve dos serviços
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }
}
