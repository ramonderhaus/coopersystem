import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SelectionType, TableColumn } from '@swimlane/ngx-datatable';
import { InvestmentsService } from '@shared/services/investments.service';
import { IInvestments, IInvestmentsList } from '@shared/models/investments';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

interface IMessages {
  [key: string]: string;
}

@Component({
  selector: 'app-investments',
  templateUrl: './investments.component.html',
  styleUrls: ['./investments.component.scss'],
  providers: [InvestmentsService]
})
export class InvestmentsComponent implements OnInit, DoCheck {

  /**
   * Recebe a lista de clientes
   */
  investments!: IInvestmentsList[] | null;

  /**
   * Define as colunas do Datatable
   */
  columns: TableColumn[] = [
    { prop: 'nome' },
    { prop: 'objetivo' },
    { prop: 'saldoTotal' }
  ];

  /**
   * Define as mensagens do Datatable
   */
  messages!: IMessages;

  /**
   * Recebe os dados da requisição dos investimentos
   */
  investmentsServiceEnd$!: Subscription;

  /**
   * Cancela as assinaturas dos observables
   */
  subscriptions: Subscription[] = []

  /**
   * Controla a exibição do conteúdo no HTML
   */
  loading: boolean = true;

  constructor(
    protected investmentsService: InvestmentsService,
    private router: Router,
    private toastr: ToastrService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.getInvestmnets();
    this.updDatatableMessage();
  }

  /**
   * Atualiza as mensagens exibidas no Datatable após a alteração do idioma
   */
  ngDoCheck(): void {
    this.updDatatableMessage();
  }

  /**
   * Atualiza as mensagens exibidas no Datatable
   */
  updDatatableMessage(): void {
    this.messages = {
      emptyMessage: this.translate.instant('emptyMessage'),
      totalMessage: this.translate.instant('totalMessage')
    };
  }

  /**
   * Chama o serviço para obter todos os investimentos
   */
  getInvestmnets(): void {
    this.investmentsServiceEnd$ = this.investmentsService.getInvestments().subscribe((investments: IInvestments) => {
      this.investments = investments.response.data.listaInvestimentos;
      this.loading = false;
    });
    this.subscriptions.push(this.investmentsServiceEnd$)
  }

  /**
   * Redireciona para a tela de edição de investimentos
   * @param row Objeto com o investimento selecionado
   */

  /**
   * Redireciona para a tela de edição de investimentos
   * @param $event Event
   */
  editInvestment($event: any): void {

    const { type, row } = $event;

    if (type === 'click')
      if (row.indicadorCarencia === 'S') {
        this.toastr.error(this.translate.instant('investmentsPopUpError'));
      } else {
        const index: number = this.findIndex(row);
        this.router.navigate(['/investments/edit/' + index]);
      }
  }

  /**
   * @param row Objeto com as informações do investimento
   * @returns Retorna o índice do investimento selecionado
   */
  findIndex(row: IInvestmentsList): number {
    return this.investments!.indexOf(row);
  }

  /**
   * Verifica se o investimento tem carência
   * @param row Objeto com as informações do investimento
   * @returns
   */
  getRowClass(row: IInvestmentsList): any {
    return { 'indicadorCarencia': row.indicadorCarencia === 'S' };
  }

  /**
   * Desinscreve dos serviços
   */
  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
  }

}
