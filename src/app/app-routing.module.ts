import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'challenge',
    pathMatch: 'full'
  },
  {
    path: 'challenge',
    loadChildren: () =>
      import('@app/secured/challenge/challenge.module').then(m => m.ChallengeModule)
  },
  {
    path: 'investments',
    loadChildren: () =>
    import('@secured/investments/investments.module').then(m => m.InvestmentsModule)
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
